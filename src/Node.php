<?php
namespace JsonStreamParser;

abstract class Node
{
	public $data;

	public function __construct($data)
	{
		$this->data = $data;
	}
}
