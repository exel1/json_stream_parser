<?php
namespace JsonStreamParser;

class JsonReader
{
	private $buffer;
	private $pos = 0;

	public function pos()
	{
		return $this->pos;
	}

	public function reset()
	{
		$this->buffer = '';
		$this->pos = 0;
	}

	public function add($string, $limit = 1000000)
	{
		$this->buffer .= $string;
		if (strlen($this->buffer) > $limit) {
			$this->buffer = substr($this->buffer, $this->pos);
			$this->pos = 0;
		}
	}

	public function getLex($lex, $lex2 = null)
	{
		$pos = $this->pos;
		$token = $this->getToken();
		if ($token === null) {return null;}
		if (!$token instanceof Lex || ($token->data !== $lex && $token->data !== $lex2)) {
			throw new Exception("expected $lex".($lex2 !== null ? " or $lex2" : "")." at $pos");
		}
		return $token;
	}

	public function getToken()
	{
		if (!preg_match("#\s*({|}|\[|\]|\"|,|:|\d+(\.\d+(e\+?-?\d+)?)?(?!\$)|false(?!\$)|true(?!\$)|null(?!\$)|\$)#is", $this->buffer, $out, PREG_OFFSET_CAPTURE, $this->pos)) {
			throw new Exception("unexpected token at $this->pos");
		}
		if ($out[0][1] !== $this->pos) {
			throw new Exception("unexpected token at $this->pos");
		}

		$token = $out[1][0];
		if (!strlen($token)) {return null;}
		if ($token === "\"") {return $this->getString($out[1][1]);}

		$this->pos = $out[1][1] + strlen($token);
		if (ctype_digit($token[0])) {return new Value((float)$token);}
		if (strcasecmp($token, 'false') === 0) {return new Value(false);}
		if (strcasecmp($token, 'true') === 0) {return new Value(true);}
		if (strcasecmp($token, 'null') === 0) {return new Value(null);}
		return new Lex($token); // [] {} : ,
	}

	private function getString($offset)
	{
		if (!preg_match("#(.*?(?<!\\\\)((\\\\\\\\)*))\"#is", $this->buffer, $out, PREG_OFFSET_CAPTURE, $offset + 1)) {
			$this->pos = $offset;
			return null;
		}
		$str = $out[1][0];
		$this->pos = $out[1][1] + strlen($str) + 1;
		return new Str($str);
	}

	public function getValue()
	{
		$pos = $this->pos;
		$token = $this->getToken();
		if ($token === null) {return null;}

		if ($token instanceof Value) {return $token;}
		if ($token->data !== '[' && $token->data !== '{') {
			throw new Exception("expected '[' or '{' at $pos");
		}
		$this->pos = $pos;
		return $token->data === '[' ? $this->getArray() : $this->getObject();
	}

	public function getArray()
	{
		$pos = $this->pos;
		$token = $this->getToken();
		if ($token === null) {return null;}
		if (!$token instanceof Lex || $token->data !== '[') {
			throw new Exception("expected '[' at $pos");
		}

		$data = [];
		while (true) {
			$value = $this->getValue();
			if ($value === null) {break;}
			$data[] = $value->data;

			$lex = $this->getToken();
			if ($lex === null) {break;}
			if (!$lex instanceof Lex || ($lex->data !== ',' && $lex->data !== ']')) {
				throw new Exception("expected ',' or ']' at $this->pos");
			}
			if ($lex->data === ']') {return new Value($data);}
		}

		$this->pos = $pos;
		return null;
	}

	public function getObject()
	{
		$pos = $this->pos;
		$token = $this->getToken();
		if ($token === null) {return null;}
		if (!$token instanceof Lex || $token->data !== '{') {
			throw new Exception("expected '{' at $pos");
		}

		$data = [];
		while (true) {
			$key = $this->getValue();
			if ($key === null) {break;}
			if (!$key instanceof Str) {
				throw new Exception("key expected at $this->pos");
			}

			$lex = $this->getToken();
			if ($lex === null) {break;}
			if (!$lex instanceof Lex || $lex->data !== ':') {
				throw new Exception("expected ':' at $this->pos");
			}

			$value = $this->getValue();
			if ($value === null) {break;}
			$data[$key->data] = $value->data;

			$lex = $this->getToken();
			if ($lex === null) {break;}
			if (!$lex instanceof Lex || ($lex->data !== ',' && $lex->data !== '}')) {
				throw new Exception("expected ',' or '}' at $this->pos");
			}
			if ($lex->data === '}') {return new Value($data);}
		}

		$this->pos = $pos;
		return null;
	}
}
