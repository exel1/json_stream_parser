```
$json = '[{"qwe": 123.45, "wer": [
1, 3,
"qwe"]}, "test", {"sdf"
:12.3e45}]';

$jsonReader = new JsonReader();

// test 1
$jsonReader->add($json);
print_r($jsonReader->getArray()->data);

// reset
$jsonReader->reset();

// test2
$jsonReader->add($json);
$jsonReader->getLex('[');
while ($value = $jsonReader->getValue()) {
  echo print_r($value->data, 1), "\n";
  $jsonReader->getLex(',', ']');
}
```


// output:
```
Array
(
    [0] => Array
        (
            [qwe] => 123.45
            [wer] => Array
                (
                    [0] => 1
                    [1] => 3
                    [2] => qwe
                )

        )

    [1] => test
    [2] => Array
        (
            [sdf] => 1.23E+46
        )

)

Array
(
    [qwe] => 123.45
    [wer] => Array
        (
            [0] => 1
            [1] => 3
            [2] => qwe
        )

)
test
Array
(
    [sdf] => 1.23E+46
)
```

------------

позволяет докидывать текст json'а кусками и зачитывать как отдельные лексемы, так и сразу целые value (массивы, объекты, строки, числа, булеаны и null)

классический вариант использования - это прочитать первую лексему `[` и потом начать в цикле читать элементы массива через `getValue()`, который возвращает либо `null`,
если в буфере нет полного значения, либо объект `Value` с прочитанным значением. на все случаи unexpected syntax бросает исключение, то есть он полноценно всё обрабатывает и учитывает.